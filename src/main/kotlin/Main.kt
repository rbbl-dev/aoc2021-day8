import java.io.File

fun main() {
    println(sumNormalizedOutputValues(File("input.txt").readText().trim()))
}

fun sumNormalizedOutputValues(input: String): Int {
    val data = parseInput(input)
    var sum = 0
    data.forEach {
        val signalMap = mapSignalsToNumbers(it.first)
        sum += it.second.fold("") { acc, s -> acc + signalMap[s.toSet()] }.toInt()
    }
    return sum
}

fun parseInput(input: String): List<Pair<List<String>, List<String>>> {
    val result = ArrayList<Pair<List<String>, List<String>>>()
    input.lines().forEach { line ->
        val patternAndOutput = line.split("|")
        result.add(Pair(patternAndOutput[0].trim().split(" "), patternAndOutput[1].trim().split(" ")))
    }
    return result
}

fun mapSignalsToNumbers(signals: List<String>): Map<Set<Char>, Int> {
    val decipheredSignals = Array(10) { "" }
    signals.forEach {
        when (it.length) {
            2 -> decipheredSignals[1] = it
            3 -> decipheredSignals[7] = it
            4 -> decipheredSignals[4] = it
            7 -> decipheredSignals[8] = it
        }
    }

    signals.filter { it.length == 5 }.forEach {
        if (it.toSet().containsAll(decipheredSignals[1].toSet())) {
            decipheredSignals[3] = it
        } else if ((it + decipheredSignals[4]).toSet().size == 7) {
            decipheredSignals[2] = it
        } else {
            decipheredSignals[5] = it
        }
    }

    signals.filter { it.length == 6 }.forEach {
        if (!it.toSet().containsAll(decipheredSignals[1].toSet())) {
            decipheredSignals[6] = it
        } else if ((decipheredSignals[3] + decipheredSignals[4]).toSet().containsAll(it.toSet())) {
            decipheredSignals[9] = it
        } else {
            decipheredSignals[0] = it
        }

    }
    val result = mutableMapOf<Set<Char>, Int>()
    decipheredSignals.forEachIndexed { index: Int, s: String ->
        result[s.toSet()] = index
    }
    return result
}